#!/usr/bin/python3

from setuptools import setup, findall

setup(
    name        = "makecert",
    description = "A minimal and easy-to-use certificate generation script",
    version     = "0.1",
    licence     = "GPLv3+",
    scripts     = ["makecert"],
)
