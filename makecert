#!/usr/bin/python3

import argparse
import json
import logging
import os
import re
import subprocess
import shlex
import sys
import tempfile

log = logging.getLogger("makecert")

def run(cmd, **kw):
    log.info("exec:  %s", " ".join(map(shlex.quote, cmd)))
    subprocess.check_call(cmd, **kw)


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("-o", "--output", metavar="BASENAME", required=True,
            help="Base name for the output files")

    parser.add_argument("--crt", action="store_true",
            help="Generate a self-signed certificate (named BASENAME.crt)")

    parser.add_argument("--req", action="store_true",
            help="Generate a certificate request (named BASENAME.req)")


    parser.add_argument("--subject",
            help='Subject (eg: "C=FR, O=..., CN=...")')

    parser.add_argument("--san", metavar="ALTNAME", action="append", default=[],
            help="Subject Alternative Name")

    parser.add_argument("--days", type=int, default=365,
            help="expiration in days")

    parser.add_argument("--ca", action="store_true",
            help="Generate a CA certificate")

    parser.add_argument("--text", action="store_true",
            help="Output textual information before PEM-encoded data")

    args = parser.parse_args()

    logging.basicConfig(level="INFO")

    if not args.crt and not args.req:
        log.error("must provide at least --crt or --req (or both)")
        sys.exit(1)


    crtpath = args.output + ".crt"
    keypath = args.output + ".key"
    reqpath = args.output + ".req"

    certtool = "certtool", ("--text" if args.text else "--no-text")

    log.info("Generating private key:             %s", keypath)
    run([*certtool, "--generate-privkey", "--outfile", keypath])

    with tempfile.NamedTemporaryFile("w+") as template:
        def addline(key, val = None):
            if val is None:
                template.write(key+"\n")
            else:
                template.write("%s = %s\n" % (key, json.dumps(val)))

        addline("expiration_days", args.days)
        if args.subject:
            addline("dn", args.subject)
        if args.ca:
            addline("ca")
        for san in args.san:
            if re.match(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\Z", san):
                addline("ip_address", san)
            elif re.match(r"[a-z][a-z0-9]*:", san):
                addline("uri", san)
            elif "@" in san:
                addline("email", san)
            else:
                addline("dns_name", san)


        template.flush()

        if args.crt:
            log.info("Generating self-signed certificate: %s", crtpath)
            run([*certtool, "--generate-self-signed", "--outfile", crtpath,
                "--load-privkey", keypath, "--template", template.name],
                stdin=subprocess.DEVNULL)
        if args.req:
            log.info("Generating certificate request: %s", reqpath)
            run([*certtool, "--generate-request", "--outfile", reqpath,
                "--load-privkey", keypath, "--template", template.name],
                stdin=subprocess.DEVNULL)

if __name__ == "__main__":
    main()
